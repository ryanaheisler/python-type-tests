from typing import List, Tuple, Optional, Callable, Sequence, Mapping, Dict, Any


print('PRIMITIVES')



# Notice return type of function is None, meaning it doesn't have to return
def primitive_parameters(boolean: bool, string: str, integer: int, floating_point: float) -> None:

    print('bool:', boolean)
    print('string:', string)
    print('integer:', integer)
    print('float:', floating_point)


b: bool = True
s: str = 'Howdy, pardner'
i: int = 5
f: float = 8.3901

primitive_parameters(b, s, i, f)

print()
print('OPTIONALS')


def optional_parameters(maybe_integer: Optional[int], maybe_string: Optional[str]):

    if maybe_integer:
        print('optional integer:', maybe_integer)
    else:
        print('OPTIONAL INTEGER NOT PRESENT')

    if maybe_string:
        print('optional string:', maybe_string)
    else:
        print('OPTIONAL STRING NOT PRESENT')


optional_int: Optional[int] = 5
optional_string: Optional[str] = None

optional_parameters(optional_int, optional_string)

print()
print('COLLECTIONS')


def collection_parameters(list_of_strings: List[str], tuple_of_ints: Tuple[int], dictionary_: Dict[str, int]):

    print('strings:', end=' ')
    for string_ in list_of_strings:
        print(string_, end=', ')
    print()

    print('ints:', end=' ')
    for integer_ in tuple_of_ints:
        print(integer_, end=', ')
    print()

    print('dictionary:')
    for key, value in dictionary_.items():
        print("  key:", key, ", value:", value)


strings: List[str] = ['a', 'b', 'c']
integers: Tuple[int] = (1, 2, 3, 4)
dictionary: Dict[str, int] = {'ryan': 1, 'matilda': 54}

collection_parameters(strings, integers, dictionary)

print()
print('MORE COLLECTIONS')


def broad_collection_type_parameters(sequence: Sequence[int], mapping: Mapping[str, int]):

    print('sequence:', end=' ')
    for thing in sequence:
        print(thing, end=', ')
    print()

    print('mapping:')
    for key, value in mapping.items():
        print("  key:", key, ", value:", value)


broad_collection_type_parameters(integers, dictionary)

print()
print('FUNCTION AS A PARAMETER')


def function_parameter(function_: Callable[[int, float], str]):

    print('calling function with 1, 3.5')
    result = function_(5, 3.3)
    print(result)


def function_to_pass_in(integer: int, float_: float) -> str:
    return '%d times %s is %s' % (integer, float_, integer * float_)


function_parameter(function_to_pass_in)

print()
print('MISC')


def other_types(any_: Any, *args: str, **kwargs: float):

    print('anything fits the Any type: ', any_)

    print('positional arguments:', end=' ')
    for arg in args:
        print(arg, end=', ')
    print()

    print('keyword arguments:')
    for key, value in kwargs.items():
        print("  key: ", key, ", value: ", value)


anything: Any = 99

arg1: str = 'one'
arg2: str = 'two'
arg3: str = 'three'

kwarg1: float = 3.141
kwarg2: float = 2.718

other_types(anything, arg1, arg2, arg3, pi=kwarg1, eulers_number=kwarg2)
